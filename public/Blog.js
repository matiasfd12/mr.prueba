//███████████████████████████████████████████████████████████████████████████████████████████████████//
//█████████████████████████████████████████████ Carga ███████████████████████████████████████████████//
//███████████████████████████████████████████████████████████████████████████████████████████████████//

function carga () {
  showSlides1(slideIndex1);
  showSlides2(slideIndex2);
  showSlides3(slideIndex3);
}

//███████████████████████████████████████████████████████████████████████████████████████████████████//
//████████████████████████████████████████████ Acordeon █████████████████████████████████████████████//
//███████████████████████████████████████████████████████████████████████████████████████████████████//

var direcciones = ['Mi_Familia', 'Donde_he_vivido', 'Donde_he_Estudiado', 'Mis_Hobbies'];
var lista = [0, 0, 0, 0];

function cargarlista(element) {
  switch (element) {
    case direcciones[0]: {
      if (lista[0]==0){
        document.getElementById(element).classList.replace('p_hide', 'p_display');
        lista[0]=1;
      }
      else {
        document.getElementById(element).classList.replace('p_display', 'p_hide');
        lista[0]=0;
      }
    }
    break;
    case direcciones[1]: {
      if (lista[1]==0){
        document.getElementById(element).classList.replace('p_hide', 'p_display');
        lista[1]=1;
      }
      else {
        document.getElementById(element).classList.replace('p_display', 'p_hide');
        lista[1]=0;
      }
    }
    break;
    case direcciones[2]: {
      if (lista[2]==0){
        document.getElementById(element).classList.replace('p_hide', 'p_display');
        lista[2]=1;
      }
      else {
        document.getElementById(element).classList.replace('p_display', 'p_hide');
        lista[2]=0;
      }
    }
    break;
    case direcciones[3]: {
      if (lista[3]==0){
        document.getElementById(element).classList.replace('p_hide', 'p_display');
        lista[3]=1;
      }
      else {
        document.getElementById(element).classList.replace('p_display', 'p_hide');
        lista[3]=0;
      }
    }
    break;
  }
}

//███████████████████████████████████████████████████████████████████████████████████████████████████//
//██████████████████████████████████████████ Primer Slide ███████████████████████████████████████████//
//███████████████████████████████████████████████████████████████████████████████████████████████████//

var slideIndex1 = 1;
var timer1;

function functiontimer1() {
  slideIndex1++;
  showSlides1(slideIndex1);
}

function plusSlides1(n) {
  clearTimeout(timer1);
  showSlides1(slideIndex1 += n);
}

function currentSlide1(n) {
  clearTimeout(timer1);
  showSlides1(slideIndex1 = n);
}

function showSlides1(n) {
  var i;
  var slides1 = document.getElementsByClassName("fade1");
  var dots1 = document.getElementsByClassName("dot1");
  if (n > slides1.length) {slideIndex1 = 1}
  if (n < 1) {slideIndex1 = slides1.length}
  for (i = 0; i < slides1.length; i++) {
      slides1[i].style.display = "none";
  }
  for (i = 0; i < dots1.length; i++) {
      dots1[i].className = dots1[i].className.replace(" active", "");
  }
  slides1[slideIndex1-1].style.display = "block"
  dots1[slideIndex1-1].className += " active";
  timer1 = setTimeout('functiontimer1()', 5000);
}

//███████████████████████████████████████████████████████████████████████████████████████████████████//
//██████████████████████████████████████████ Segunda Slide ██████████████████████████████████████████//
//███████████████████████████████████████████████████████████████████████████████████████████████████//

var slideIndex2 = 1;
var timer2;

function functiontimer2() {
  slideIndex2++;
  showSlides2(slideIndex2);
}

function plusSlides2(n) {
  clearTimeout(timer2);
  showSlides2(slideIndex2 += n);
}

function currentSlide2(n) {
  clearTimeout(timer2);
  showSlides2(slideIndex2 = n);
}

function showSlides2(n) {
  var i;
  var slides2 = document.getElementsByClassName("fade2");
  var dots2 = document.getElementsByClassName("dot2");
  if (n > slides2.length) {slideIndex2 = 1}
  if (n < 1) {slideIndex2 = slides2.length}
  for (i = 0; i < slides2.length; i++) {
      slides2[i].style.display = "none";
  }
  for (i = 0; i < dots2.length; i++) {
      dots2[i].className = dots2[i].className.replace(" active", "");
  }
  slides2[slideIndex2-1].style.display = "block"
  dots2[slideIndex2-1].className += " active";
  timer2 = setTimeout('functiontimer2()', 5000);
}

//███████████████████████████████████████████████████████████████████████████████████████████████████//
//██████████████████████████████████████████ Tercer Slide ███████████████████████████████████████████//
//███████████████████████████████████████████████████████████████████████████████████████████████████//

var slideIndex3 = 1;
var timer3;

function functiontimer3() {
  slideIndex3++;
  showSlides3(slideIndex3);
}

function plusSlides3(n) {
  clearTimeout(timer3);
  showSlides3(slideIndex3 += n);
}

function currentSlide3(n) {
  showSlides3(slideIndex3 = n);
}

function showSlides3(n) {
  var i;
  var slides3 = document.getElementsByClassName("fade3");
  var dots3 = document.getElementsByClassName("dot3");
  if (n > slides3.length) {slideIndex3 = 1}
  if (n < 1) {slideIndex3 = slides3.length}
  for (i = 0; i < slides3.length; i++) {
      slides3[i].style.display = "none";
  }
  for (i = 0; i < dots3.length; i++) {
      dots3[i].className = dots3[i].className.replace(" active", "");
  }
  slides3[slideIndex3-1].style.display = "block"
  dots3[slideIndex3-1].className += " active";
  timer3 = setTimeout('functiontimer3()', 5000);
}